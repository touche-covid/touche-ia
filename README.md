# Touché Covid IA

This TensorFlow Lite model/experiment is part of [our](#Team) contribution to the Covid-19 crisis. The model is a piece of a larger solution presented at a hackathon/startupweekend - April 2020.

Ultimately this model will be integrated within the [Android app](https://bitbucket.org/touche-covid/touche-android).

## Installation

TODO: If any installation steps are required please document them.

## Usage and Limitations

TODO: How to run a quick test/demo of the model.

## Team

  - Andry
  - Anupam
  - Ialifinaritra
  - Nassim
  - Pierre
  - Voahary

## License

[MIT](https://choosealicense.com/licenses/mit/)

