package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.Tensor;
import org.tensorflow.lite.support.common.FileUtil;
import org.tensorflow.lite.support.common.TensorOperator;
import org.tensorflow.lite.support.common.TensorProcessor;
import org.tensorflow.lite.support.common.ops.NormalizeOp;
import org.tensorflow.lite.support.image.ImageProcessor;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.image.ops.*;
import org.tensorflow.lite.support.label.TensorLabel;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;
import org.tensorflow.lite.support.model.Model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Interpreter tflite;

    TensorImage bitmapToTensor(Bitmap bitmap) {
        // Initialization code
        // Create an ImageProcessor with all ops required. For more ops, please
        // refer to the ImageProcessor Architecture section in this README.
        ImageProcessor imageProcessor = new ImageProcessor.Builder()
            .add(new ResizeOp(260, 260, ResizeOp.ResizeMethod.BILINEAR))
            .build();

        TensorImage tImage = new TensorImage(DataType.FLOAT32);

        // Analysis code for every frame
        tImage.load(bitmap);
        return imageProcessor.process(tImage);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadModel("face_mask_detection.tflite");

        Button btn = findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.pierre_clean);
                TensorBuffer probabilities = runInference(image);

                // showing brutally all results…
                StringBuilder sb = new StringBuilder();
                float threashold = 0.0001f;
                sb.append("Results greater than "+threashold+"\n");
                int count = 0;
                int size = probabilities.getFlatSize();
                for(int i = 0; i < size; i++) {
                    float p = probabilities.getIntValue(i);
                    if (p > threashold) {
                        count += 1;
                        sb.append("P[");
                        sb.append(i);
                        sb.append("]=");
                        sb.append(p);
                        sb.append("\n");
                    }
                }
                Log.i("TEST",  sb.toString());
                Log.i("TEST",  "Count = " + count + " over " + size + "\n");
            }
        });
    }

    boolean loadModel(String filepath) {
        // Initialise the model
        try {
            MappedByteBuffer tfliteModel = FileUtil.loadMappedFile(this, filepath);
            tflite = new Interpreter(tfliteModel);
            return true;
        } catch (IOException e) {
            Log.e("tfliteSupport", "Error reading model", e);
            return false;
        }
    }

    TensorBuffer runInference(Bitmap image) {
        assert(tflite != null);

        TensorImage inputImage = bitmapToTensor(image);
        TensorBuffer[] outputs = {
                TensorBuffer.createFixedSize(new int[]{1, 5972, 4}, DataType.FLOAT32),
                TensorBuffer.createFixedSize(new int[]{1, 5972, 2}, DataType.FLOAT32)
        };
        Map<Integer, Object> outputsMap = new HashMap();
        for(int i = 0; i < outputs.length; i++) { outputsMap.put(i, outputs[i].getBuffer()); }

        tflite.runForMultipleInputsOutputs(new Object[]{inputImage.getBuffer()}, outputsMap);
        return outputs[1];
    }
}
